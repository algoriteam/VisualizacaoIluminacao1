﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.IO;

public class Tools {

    private static System.Random random = new System.Random();

    public static string RandomString(int length)
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
    }

    public static int[] SubArray (int[] data, int index, int length)
    {
        int[] result = new int[length];
        System.Array.Copy(data, index, result, 0, length);
        return result;
    }

    public static List<Lot> lotExtraction(int[][] road_map, int w, int l, string seed, bool write)
    {
        List<Lot> result = new List<Lot>();

        int last_area = 9;
        int index = 0;

        for(int i = 0; i < w; i++)
        {
            for(int j = 0; j < l; j++)
            {
                int value = road_map[i][j];

                // We're in a FREE AREA
                if (value == 0)
                {
                    // Bottom left
                    int n1 = road_map[i + 1][j - 1];
                    // Bottom
                    int n2 = road_map[i + 1][j];
                    // Bottom right
                    int n3 = road_map[i + 1][j + 1];
                    // Right
                    int n4 = road_map[i][j + 1];
                    // Top right
                    int n5 = road_map[i - 1][j + 1];
                    // Top
                    int n6 = road_map[i - 1][j];
                    // Top left
                    int n7 = road_map[i - 1][j - 1];
                    // Left
                    int n8 = road_map[i][j - 1];

                    // Left top corner
                    if (isWall(n8, n7, n6))
                    {
                        last_area++;
                        index = last_area - 10;

                        road_map[i][j] = last_area;
                        addVector(result, i, j, index);
                    }
                    else
                    {
                        // Right top corner
                        if (isWall(n6, n5, n4))
                        {
                            road_map[i][j] = n8;
                            index = n8 - 10;
                            addVector(result, i, j, index);
                        }
                        else
                        {
                            // Right bottom corner
                            // Left bottom corner
                            if (isWall(n4, n3, n2) || isWall(n2, n1, n8))
                            {
                                road_map[i][j] = n6;
                                index = n6 - 10;
                                addVector(result, i, j, index);
                            }
                            else
                            {
                                // Mid up
                                if (isWall(n7, n6, n5))
                                {
                                    road_map[i][j] = n8;
                                }
                                // Mid down
                                // Mid left
                                // Mid right
                                else
                                {
                                    road_map[i][j] = n6;
                                }
                            }
                        }
                    }
                }

            }
        }

        if (write)
        {
            TextWriter tw = new StreamWriter("Lot_Map_" + seed + ".txt");
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < l; j++)
                {
                    tw.Write(" ");
                    tw.Write(road_map[i][j]);
                }
                tw.WriteLine();
            }
            tw.Flush();
        }

        return result;
    }

    private static bool isWall(int a)
    {
        return a > 0 && a < 4;
    }

    private static bool isWall(int a, int b, int c)
    {
        return a > 0 && b > 0 && c > 0 && a < 4 && b < 4 && c < 4;
    }

    private static void addVector(List<Lot> lots, int w, int l, int index)
    {
        // Check if lot exists, if not create it
        if(lots.ElementAtOrDefault(index) == null)
        {
            lots.Add(new Lot());
        }

        Vector3 v = new Vector3(w, 0.0f, l);
        lots.ElementAtOrDefault(index).addVector(v);
    }

    public static int[][] roadLSystem(int maxX, int maxZ, int gLevel, string seed, bool write)
    {
        int seed_num = seed.Sum(b => b);
        Random.InitState(seed_num);

        int[][] grelhaMapa = new int[maxX][];
        int z = 0, x = 0, i, j;

        //Inicializar Grelha a zeros com ruas já nos limites do mapa
        for (i = 0; i < maxX; i++)
        {
            grelhaMapa[i] = new int[maxZ];
            for (j = 0; j < maxZ; j++)
            {
                //Tratamentos de cantos do mapa
                if ((i == 0 && (j == 0 || j == maxX - 1)) || (i == maxX - 1 && (j == 0 || j == maxZ - 1))) grelhaMapa[i][j] = 3;
                //Tratamento de limites à esquera e direita
                else if (j == 0 || j == maxZ - 1) grelhaMapa[i][j] = 1;
                //Tratamento de limites inferiores e superiores
                else if (i == 0 || i == maxX - 1) grelhaMapa[i][j] = 2;
                //Inicializar restantes condições a zero
                else grelhaMapa[i][j] = 0;
            }
        }

        if (gLevel > 1)// Tratamento do 2º nível que consiste apenas numa rua vertical
        {
            //Escolher aleatoriamente coluna do mapa para formar rua 
            z = Random.Range(2, maxZ - 3);
            x = 0;
            gLevel--;

            //formar cruzamento com limite superior
            grelhaMapa[x][z] = 3;

            //contruir rua do limite superior ao limite inferior
            for (i = x + 1; i < maxX - 1 && grelhaMapa[i][z] == 0; i++)
            {
                grelhaMapa[i][z] = 1;
            }

            //formar cruzamento com limite inferior
            grelhaMapa[i][z] = 3;

            //Verificação de quantidade de níveis e espaçamento para criação de novas ruas
            if (gLevel > 1 && 3 < maxX - 3)
            {
                //escolher aleatoriamente duas linhas na mesma posição de coluna para novas ruas
                growHe(grelhaMapa, Random.Range(2, maxX - 3), z, maxX, maxZ, gLevel - 1);
                growHd(grelhaMapa, Random.Range(2, maxX - 3), z, maxX, maxZ, gLevel - 1);
            }
        }

        if (write)
        {
            TextWriter tw = new StreamWriter("Road_Map_" + seed + ".txt");
            for (i = 0; i < maxX; i++)
            {
                for (j = 0; j < maxZ; j++)
                {
                    tw.Write(" ");
                    tw.Write(grelhaMapa[i][j]);
                }
                tw.WriteLine();
            }
            tw.Flush();
        }

        return grelhaMapa;
    }

    private static void growHe(int[][] grelhaMapa, int x, int z, int maxX, int maxZ, int gLevel)
    {
        int j;

        //criar cruzamento a partir do limite direito da rua a formar
        grelhaMapa[x][z] = 3;

        //criar espaçamento ao cruzamento criado
        if (z - 1 > 0) grelhaMapa[x][z - 1] = 2;

        //criar rua até encontrar limite esquerdo
        for (j = z - 2; j > 0 && grelhaMapa[x][j] == 0; j--)
        {
            grelhaMapa[x][j] = 2;
        }
        //criar cruzamento no limite esquerdo encontrado
        grelhaMapa[x][j] = 3;

        //Verificação de quantidade de níveis e espaçamento para criação de novas ruas
        if (gLevel > 1 && j + 2 <= z - 2)
        {
            //escolher aleatoriamente duas colunas na mesma posição de linha para novas ruas
            growVs(grelhaMapa, x, Random.Range(j + 2, z - 2), maxX, maxZ, gLevel - 1);
            growVi(grelhaMapa, x, Random.Range(j + 2, z - 2), maxX, maxZ, gLevel - 1);
        }

    }

    private static void growHd(int[][] grelhaMapa, int x, int z, int maxX, int maxZ, int gLevel)
    {
        int j;

        //criar cruzamento a partir do limite esquerdo da rua a formar
        grelhaMapa[x][z] = 3;

        //criar espaçamento ao cruzamento criado
        if (z + 1 < maxZ - 1) grelhaMapa[x][z + 1] = 2;

        //criar rua até encontrar limite direito
        for (j = z + 2; j < maxZ - 1 && grelhaMapa[x][j] == 0; j++)
        {
            grelhaMapa[x][j] = 2;
        }

        //criar cruzamento no limite direito encontrado
        grelhaMapa[x][j] = 3;

        //Verificação de quantidade de níveis e espaçamento para criação de novas ruas
        if (gLevel > 1 && z + 2 <= j - 2)
        {
            //escolher aleatoriamente duas colunas na mesma posição de linha para novas ruas
            growVs(grelhaMapa, x, Random.Range(z + 2, j - 2), maxX, maxZ, gLevel - 1);
            growVi(grelhaMapa, x, Random.Range(z + 2, j - 2), maxX, maxZ, gLevel - 1);
        }

    }

    private static void growVs(int[][] grelhaMapa, int x, int z, int maxX, int maxZ, int gLevel)
    {
        int i;

        //criar cruzamento a partir do limite inferior da rua a formar
        grelhaMapa[x][z] = 3;

        //criar espaçamento ao cruzamento criado
        if (x - 1 > 0) grelhaMapa[x - 1][z] = 1;

        //criar rua até encontrar limite superior
        for (i = x - 2; i > 0 && grelhaMapa[i][z] == 0; i--)
        {
            grelhaMapa[i][z] = 1;
        }

        //criar cruzamento no limite superior encontrado
        grelhaMapa[i][z] = 3;

        //Verificação de quantidade de níveis e espaçamento para criação de novas ruas
        if (gLevel > 1 && i + 2 < x - 2)
        {
            //escolher aleatoriamente duas linhas na mesma posição de coluna para novas ruas
            growHe(grelhaMapa, Random.Range(i + 2, x - 2), z, maxX, maxZ, gLevel - 1);
            growHd(grelhaMapa, Random.Range(i + 2, x - 2), z, maxX, maxZ, gLevel - 1);
        }

    }

    private static void growVi(int[][] grelhaMapa, int x, int z, int maxX, int maxZ, int gLevel)
    {
        int i;

        //criar cruzamento a partir do limite superior da rua a formar
        grelhaMapa[x][z] = 3;

        //criar espaçamento ao cruzamento criado
        if (x + 1 < maxX - 1) grelhaMapa[x + 1][z] = 1;

        //criar rua até encontrar limite inferior
        for (i = x + 2; i < maxX - 1 && grelhaMapa[i][z] == 0; i++)
        {
            grelhaMapa[i][z] = 1;
        }
        //criar cruzamento no limite inferior encontrado
        grelhaMapa[i][z] = 3;

        //Verificação de quantidade de níveis e espaçamento para criação de novas ruas
        if (gLevel > 1 && x + 2 < i - 2)
        {
            //escolher aleatoriamente duas linhas na mesma posição de coluna para novas ruas
            growHe(grelhaMapa, Random.Range(x + 2, i - 2), z, maxX, maxZ, gLevel - 1);
            growHd(grelhaMapa, Random.Range(x + 2, i - 2), z, maxX, maxZ, gLevel - 1);
        }

    }

}

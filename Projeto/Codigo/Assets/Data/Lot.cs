﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Lot {

    // Starting point
    private Vector3 start;

    // Area denoted by 4 points
    private List<Vector3> vertices;

    private float width;
    private float length;

    // House buildings limits
    private float building_height;
    private float building_width;
    private float building_length;

    /* Lot category
         0 - Recidencial
         1 - Comercial
         2 - Skysraper
    */
    private int type;

    // Drawing building points
    private List<Vector3> building_points;

    private GameObject tiles_parent;
    private List<Tile> building_tiles;

    // Drawing tile + corner
    private Tile tile;

    private GameObject corners_parent;
    private List<Tile> corners;

    // Drawing buildings
    private GameObject buildings_parent;
    private List<Building> buildings;

    public Lot()
    {
        // Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3
        this.vertices = new List<Vector3>();

        this.width = 0.0f;
        this.length = 0.0f;

        this.building_height = 0.0f;
        this.building_length = 0.0f;
        this.building_width = 0.0f;

        this.type = -1;

        this.building_points = new List<Vector3>();
        this.building_tiles = new List<Tile>();

        this.corners = new List<Tile>();
        this.buildings = new List<Building>();
    }

    public void setSize(Vector3 s)
    {
        this.start = s;

        correctVertices();

        Vector3 vertice_0 = this.vertices[0];
        Vector3 vertice_1 = this.vertices[1];
        Vector3 vertice_2 = this.vertices[2];

        setWidth(Vector3.Distance(vertice_0, vertice_2));
        setLength(Vector3.Distance(vertice_0, vertice_1));

        setTile();
        setCorners();
    }

    private void correctVertices()
    {
        float half_size = 0.5f;

        Vector3 point_0 = this.vertices[0];
        Vector3 point_1;
        Vector3 point_2;
        Vector3 point_3;

        // Check if area has only 2 or more vertices
        switch (this.vertices.Count)
        {
            case 1:
                point_1 = this.vertices[0];
                addVector(this.vertices[0]);
                addVector(this.vertices[0]);
                addVector(this.vertices[0]);
                break;
            case 2:
                point_1 = this.vertices[1];
                if (point_0.x != point_1.x)
                {
                    point_1 = this.vertices[0];
                    addVector(this.vertices[1]);
                    addVector(this.vertices[1]);
                }
                else
                {
                    point_1 = this.vertices[1];
                    addVector(this.vertices[0]);
                    addVector(this.vertices[1]);
                }
                break;
            default:
                point_1 = this.vertices[1];
                break;
        }

        point_2 = this.vertices[2];
        point_3 = this.vertices[3];

        this.vertices[0] = new Vector3(point_0.x - half_size, point_0.y, point_0.z - half_size);
        this.vertices[1] = new Vector3(point_1.x - half_size, point_1.y, point_1.z + half_size);
        this.vertices[2] = new Vector3(point_2.x + half_size, point_2.y, point_2.z - half_size);
        this.vertices[3] = new Vector3(point_3.x + half_size, point_3.y, point_3.z + half_size);
    }

    private void setTile()
    {
        Vector3 final = new Vector3(this.start.x + this.vertices[0].x, 0.0f, this.start.z + this.vertices[0].z);
        this.tile = new Tile(final, Quaternion.identity, this.width, this.length);
    }

    private void setCorners()
    {
        Vector3 point_0 = new Vector3(this.start.x + this.vertices[0].x, 0.0f, this.start.z + this.vertices[0].z);
        Vector3 point_1 = new Vector3(this.start.x + this.vertices[1].x, 0.0f, this.start.z + this.vertices[1].z - 1.0f);
        Vector3 point_2 = new Vector3(this.start.x + this.vertices[2].x - 1.0f, 0.0f, this.start.z + this.vertices[2].z);
        Vector3 point_3 = new Vector3(this.start.x + this.vertices[3].x - 1.0f, 0.0f, this.start.z + this.vertices[3].z - 1.0f);

        Tile corner_0 = new Tile(point_0, Quaternion.identity, 1.0f, 1.0f);
        corner_0.setMaterial("PINK");

        Tile corner_1 = new Tile(point_1, Quaternion.identity, 1.0f, 1.0f);
        corner_1.setMaterial("PINK");

        Tile corner_2 = new Tile(point_2, Quaternion.identity, 1.0f, 1.0f);
        corner_2.setMaterial("PINK");

        Tile corner_3 = new Tile(point_3, Quaternion.identity, 1.0f, 1.0f);
        corner_3.setMaterial("PINK");

        this.corners.Add(corner_0);
        this.corners.Add(corner_1);
        this.corners.Add(corner_2);
        this.corners.Add(corner_3);
    }

    // GETS

    public Vector3 getCornerPoint()
    {
        return this.vertices[0];
    }

    public List<Vector3> getVerticesPointer()
    {
        return this.vertices;
    }

    public float getLength()
    {
        return this.length;
    }

    public float getWidth()
    {
        return this.width;
    }

    public float getBuildingHeight()
    {
        return this.building_height;
    }

    public float getBuildingLength()
    {
        return this.building_length;
    }

    public float getBuildingWidth()
    {
        return this.building_width;
    }

    public int getType()
    {
        return this.type;
    }

    public List<Vector3> getBuildingPointsPointer()
    {
        return this.building_points;
    }

    public List<Tile> getBuildingTilesPointer()
    {
        return this.building_tiles;
    }

    public Tile getTile()
    {
        return this.tile;
    }

    public List<Tile> getCorners()
    {
        return this.corners;
    }

    public List<Building> getBuildingsPointer()
    {
        return this.buildings;
    }

    // SETS

    public void setLength(float l)
    {
        this.length = l;
    }

    public void setWidth(float w)
    {
        this.width = w;
    }

    public void setBuildingHeight(float h)
    {
        this.building_height = h;
    }

    public void setBuildingLength(float l)
    {
        this.building_length = l;
    }

    public void setBuildingWidth(float w)
    {
        this.building_width = w;
    }

    public void setType(int t)
    {
        this.type = t;
    }

    // OTHER METHODS

    public void addVector(Vector3 v)
    {
        this.vertices.Add(v);
    }

    public void setBuildingCategory(int t, float w, float l, float h)
    {
        setType(t);
        setBuildingWidth(w);
        setBuildingLength(l);
        setBuildingHeight(h);

        switch (this.type)
        {
            case 0:
                this.tile.setMaterial("BLUE");
                break;
            case 1:
                this.tile.setMaterial("ORANGE");
                break;
            case 2:
                this.tile.setMaterial("GREEN");
                break;
            default:
                this.tile.setMaterial("GRAY");
                break;
        }
    }

    public void setBuildingPoints()
    {
        float safe_w;
        float safe_l;

        float w_offset;
        float l_offset;

        int points_per_row;
        int points_per_column;

        float row_distance;
        float column_distance;
        float jump_distance_row;
        float jump_distance_column;

        // Safe area
        w_offset = this.building_width * 0.2f;
        l_offset = this.building_length * 0.2f;

        safe_w = this.width - 2 * w_offset;
        safe_l = this.length - 2 * l_offset;

        // Points number
        points_per_column = Mathf.FloorToInt(safe_w / (this.building_width + 1.0f));
        points_per_row = Mathf.FloorToInt(safe_l / (this.building_length + 1.0f));

        if(points_per_row > 0 && points_per_column > 0)
        {
            // Area is not empty
            Vector3 vertice_0 = this.vertices[0] + new Vector3(w_offset, 0.0f, l_offset);
            Vector3 vertice_1 = this.vertices[1] + new Vector3(w_offset, 0.0f, -l_offset);
            Vector3 vertice_2 = this.vertices[2] + new Vector3(-w_offset, 0.0f, l_offset);
            //Vector3 vertice_3 = this.vertices[3] + new Vector3(-w_offset, 0.0f, -l_offset);

            row_distance = Vector3.Distance(vertice_0, vertice_1);
            column_distance = Vector3.Distance(vertice_0, vertice_2);

            jump_distance_row = (row_distance / (points_per_row + 1));
            jump_distance_column = (column_distance / (points_per_column + 1));

            for (int i = 1; i <= points_per_column; i++)
            {
                for(int j = 1; j <= points_per_row; j++)
                {
                    Vector3 p = new Vector3(vertice_0.x + (i * jump_distance_column), vertice_0.y, vertice_0.z + (j * jump_distance_row));
                    addPoint(p);
                }
            }
        }
        else
        {
            // Area is empty
        }
    }

    private void addPoint(Vector3 p)
    {
        Tile t = new Tile(new Vector3(this.start.x + p.x, 0.0f, this.start.z + p.z), Quaternion.identity, 0.2f, 0.2f);
        switch (this.type)
        {
            case 0:
                t.setMaterial("BLUE");
                break;
            case 1:
                t.setMaterial("ORANGE");
                break;
            case 2:
                t.setMaterial("GREEN");
                break;
            default:
                t.setMaterial("GRAY");
                break;
        }
        this.building_points.Add(new Vector3(this.start.x + p.x, 0.0f, this.start.z + p.z));
        this.building_tiles.Add(t);
    }

    public void setBuildings(int seed)
    {
        Random.InitState(seed);

        float random_width = 1.0f;
        float random_length = 1.0f;
        float random_height = 1.0f;

        foreach (Vector3 p in this.building_points)
        {
            random_width = Random.value * (this.building_width - 1) + 1.0f;
            random_length = Random.value * (this.building_length - 1) + 1.0f;
            random_height = Random.value * (this.building_height - 1) + 1.0f;

            addBuilding(new Building(p, Quaternion.identity, this.type, random_width, random_length, random_height));
        }
    }

    private void addBuilding(Building b)
    {
        this.buildings.Add(b);
    }

    public bool hasBuildings()
    {
        return this.buildings.Count > 0;
    }

    // DRAWING METHODS

    public GameObject getCornersParent()
    {
        return this.corners_parent;
    }

    public void setCornersParent(GameObject p)
    {
        this.corners_parent = p;
    }

    public GameObject getTilesParent()
    {
        return this.tiles_parent;
    }

    public void setTilesParent(GameObject p)
    {
        this.tiles_parent = p;
    }

    public GameObject getBuildingsParent()
    {
        return this.buildings_parent;
    }

    public void setBuildingsParent(GameObject p)
    {
        this.buildings_parent = p;
    }

    // Corner
    public void drawCorners()
    {
        this.corners_parent = new GameObject("Corners Group per Lot");
        List<GameObject> objects = new List<GameObject>();
    
        foreach (Tile t in this.corners)
        {
            objects.Add(t.getObject());
            t.draw();
        }

        StaticBatchingUtility.Combine(objects.ToArray(), this.corners_parent);
    }

    public void undrawCorner()
    {
        foreach (Tile t in this.corners)
        {
            t.undraw();
        }
    }

    public void setCornerMaterial(string c)
    {
        foreach (Tile t in this.corners)
        {
            t.setMaterial(c);
        }
    }

    // Tile
    public void drawTile()
    {
        this.tile.draw();
    }

    public void undrawTile()
    {
        this.tile.undraw();
    }

    public void setTileMaterial(string c)
    {
        this.tile.setMaterial(c);
    }

    // Building points
    public void drawBuildingPoints()
    {
        this.tiles_parent = new GameObject("Building Points Group per Lot");
        List<GameObject> objects = new List<GameObject>();

        foreach (Tile t in this.building_tiles)
        {
            objects.Add(t.getObject());
            t.draw();
        }

        StaticBatchingUtility.Combine(objects.ToArray(), this.tiles_parent);
    }

    public void undrawBuildingPoints()
    {
        foreach (Tile t in this.building_tiles)
        {
            t.undraw();
        }
    }

    public void setBuildingPointsMaterial(string c)
    {
        foreach (Tile t in this.building_tiles)
        {
            t.setMaterial(c);
        }
    }

    // Buildings
    public void drawBuildings()
    {
        this.buildings_parent = new GameObject("Buildings Group per Lot");
        List<GameObject> objects = new List<GameObject>();

        foreach (Building b in this.buildings)
        {
            objects.Add(b.getObject());
            b.draw();
        }

        StaticBatchingUtility.Combine(objects.ToArray(), this.buildings_parent);
    }

    public void undrawBuildings()
    {
        foreach (Building b in this.buildings)
        {
            b.undraw();
        }
    }

    public void setBuildingsMaterial(string c)
    {
        foreach (Building b in this.buildings)
        {
            b.setMaterial(c);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;

public class Map : MonoBehaviour {

    private bool roads_drawn;
    private bool lots_drawn;

    // Ambient Variables

    // Starting point
        public Vector3 start;

        // Random seed
        public string seed;
        private int seed_num;

        // Plane
        public int width;
        public int length;

        // Max building height
        public float max_building_height;
        public float residential_height_percentage;
        public float comercial_height_percentage;
        public float skyscraper_height_percentage;

        // Building length and height
        public float max_residential_width;
        public float max_residential_length;

        public float max_comercial_width;
        public float max_comercial_length;

        public float max_skyscraper_width;
        public float max_skyscraper_length;

        // L-System number of generations
        public int road_generation_depth;

    // Perlin noise normalised 2D matrix
    private float[][] height_map;

    // L-System 2D road matrix
    private int[][] road_map;

    // Game Objects

        // Roads
        private GameObject roads_parent;
        private List<Road> roads;

        // Lots
        private List<Lot> city_lots;

    public Map(Vector3 p, int w, int l, float max_h, float r_l, float r_w, float r_exp, float c_l, float c_w, float c_exp, float s_l, float s_w, float s_exp, int gen, string s)
    {
        this.start = p;

        // Random seed
        this.seed = s;
        this.seed_num = s.Sum(b => b);

        // City map plane size
        this.width = w;
        this.length = l;

        // City buildings structure height
        this.max_building_height = max_h;
        this.residential_height_percentage = r_exp;
        this.comercial_height_percentage = c_exp;
        this.skyscraper_height_percentage = s_exp;

        // City buildings structure length and width
        this.max_residential_length = r_l;
        this.max_residential_width = r_w;
        this.max_comercial_length = c_l;
        this.max_comercial_width = c_w;
        this.max_skyscraper_length = s_l;
        this.max_skyscraper_width = s_w;

        // L-System roads generations depth
        this.road_generation_depth = gen;

        // Initiate roads
        this.roads = new List<Road>();

        // Initiate Lots
        this.city_lots = new List<Lot>();

        // Already drawn
        this.roads_drawn = false;
        this.lots_drawn = false;
    }

    public void preloadCity()
    {
        // 1 - Create road LSystem
        createLSystem(false);

        // 2 - Generate roads (Includes city limits)
        generateRoads();

        // 3 - Draw roads
        //drawRoads();

        // 4 - Extract available lots to build on
        generateLots(false);

        // 5 - Create height map (Use perlin filter)
        generateHeightMap(false);

        // 6 - Apply filter for each city lot (Assign category, type, calculate width, length and height)
        assignLots();

        // 7 - Draw lot corners
        //drawLotCorners();

        // 8 - Draw lot category tile map
        //clearCorners();
        //drawLotTiles();

        // 9 - Generate available building points in each lot
        //undrawLotTiles();
        generateBuildingPoints();

        // 10 - Draw available building points
        //drawBuildingPoints();

        // 11 - Generate buildings
        //clearBuildingPoints();
        generateBuildings();

        // 12 - Draw buildings
        //drawBuildings();
    }

    public void generateCity()
    {
        // 1 - Create road LSystem
        createLSystem(false);

        // 2 - Generate roads (Includes city limits)
        generateRoads();

        // 3 - Draw roads
        drawRoads();

        // 4 - Extract available lots to build on
        generateLots(false);

        // 5 - Create height map (Use perlin filter)
        generateHeightMap(false);

        // 6 - Apply filter for each city lot (Assign category, type, calculate width, length and height)
        assignLots();

        // 7 - Draw lot corners
        //drawLotCorners();

        // 8 - Draw lot category tile map
        //clearCorners();
        //drawLotTiles();

        // 9 - Generate available building points in each lot
        //undrawLotTiles();
        generateBuildingPoints();

        // 10 - Draw available building points
        //drawBuildingPoints();

        // 11 - Generate buildings
        //clearBuildingPoints();
        generateBuildings();

        // 12 - Draw buildings
        drawBuildings();
    }

    // GETS

    // SETS

    // EXTRAS

    // ----- UNDRAW

    public void undrawAll()
    {
        undrawLots();
        undrawRoads();
    }

    public void undrawLots()
    {
        foreach (Lot l in this.city_lots)
        {
            Tile t = l.getTile();
            t.undraw();

            List<Building> buildings = l.getBuildingsPointer();
            foreach (Building b in buildings)
            {
                b.undraw();
            }
        }
    }

    public void undrawRoads()
    {
        foreach (Road r in this.roads)
        {
            r.undraw();
        }
    }

    // ----- REDRAW

    public void redrawAll()
    {
        redrawLots();
        redrawRoads();
    }

    public void redrawLots()
    {
        if (this.lots_drawn)
        {
            foreach (Lot l in this.city_lots)
            {
                Tile t = l.getTile();
                t.draw();

                List<Building> buildings = l.getBuildingsPointer();
                foreach (Building b in buildings)
                {
                    b.draw();
                }
            }
        } else
        {
            drawBuildings();
        }
    }

    public void redrawRoads()
    {
        if (this.roads_drawn) {
            foreach (Road r in this.roads)
            {
                r.draw();
            }
        } else {
            drawRoads();
        }
    }

    // ----- CLEAR

    public void clearAll()
    {
        clearLots();
        clearRoads();

        foreach (Lot l in this.city_lots)
        {
            Destroy(l.getCornersParent());
            Destroy(l.getTilesParent());
            Destroy(l.getBuildingsParent());
        }

        Destroy(this.roads_parent);
    }

    public void clearLots()
    {
        foreach (Lot l in this.city_lots)
        {
            Tile i = l.getTile();
            Destroy(i.getObject());

            List<Tile> corners = l.getCorners();
            foreach (Tile t in corners)
            {
                Destroy(t.getObject());
            }

            List<Tile> tiles = l.getBuildingTilesPointer();
            foreach (Tile t in tiles)
            {
                Destroy(t.getObject());
            }

            List<Building> buildings = l.getBuildingsPointer();
            foreach (Building b in buildings)
            {
                Destroy(b.getObject());
            }
        }
    }

    public void clearCorners()
    {
        foreach (Lot l in this.city_lots)
        {
            List<Tile> corners = l.getCorners();
            foreach (Tile t in corners)
            {
                Destroy(t.getObject());
            }
        }
    }

    public void clearBuildingPoints()
    {
        foreach (Lot l in this.city_lots)
        {
            List<Tile> tiles = l.getBuildingTilesPointer();
            foreach (Tile t in tiles)
            {
                Destroy(t.getObject());
            }
        }
    }

    public void addRoad(Road r)
    {
        this.roads.Add(r);
    }

    public void clearRoads()
    {
        foreach(Road r in this.roads)
        {
            Destroy(r.getObject());
        }
    }

    // 1 - Create road LSystem
    public void createLSystem(bool write)
    {
        this.road_map = Tools.roadLSystem(this.width, this.length, this.road_generation_depth, this.seed, write);
    }

    // 2 - Generate roads (Includes city limits)
    public void generateRoads()
    {
        if (this.road_map != null)
        {
            for (int i = 0; i < this.width; i++)
            {
                for (int j = 0; j < this.length; j++)
                {
                    int value = this.road_map[i][j];

                    switch (value)
                    {
                        case 1:
                            // Vertical road
                            addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.identity, 1));
                            break;
                        case 2:
                            // Horizontal road
                            addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.Euler(0.0f, 90.0f, 0.0f), 1));
                            break;
                        case 3:
                            int n1 = 0;
                            int n2 = 0;
                            int n3 = 0;
                            int n4 = 0;

                            if (i > 0) n1 = this.road_map[i - 1][j];
                            if (j + 1 < this.length ) n2 = this.road_map[i][j + 1];
                            if (i + 1 < this.width) n3 = this.road_map[i + 1][j];
                            if (j > 0) n4 = this.road_map[i][j - 1];

                            // Intersection road
                            // 3
                            if (n1 > 0 && n2 > 0 && n3 == 0 && n4 > 0)
                            {
                                addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.identity, 3));
                            }
                            else
                            {
                                if (n1 > 0 && n2 > 0 && n3 > 0 && n4 == 0)
                                {
                                    addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.Euler(0.0f, 90.0f, 0.0f), 3));
                                }
                                else
                                {
                                    if (n1 == 0 && n2 > 0 && n3 > 0 && n4 > 0)
                                    {
                                        addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.Euler(0.0f, 180.0f, 0.0f), 3));
                                    }
                                    else
                                    {
                                        if (n1 > 0 && n2 == 0 && n3 > 0 && n4 > 0)
                                        {
                                            addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.Euler(0.0f, -90.0f, 0.0f), 3));
                                        }
                                        else
                                        {
                                            // (n1 > 0 && n2 > 0 && n3 > 0 && n4 > 0)
                                            addRoad(new Road(new Vector3(this.start.x + i, 0.0f, this.start.z + j), Quaternion.identity, 2));
                                        }
                                    }
                                }
                            }
                            break;
                    }
                }
            }
        }

    }

    // 3 - Draw roads
    public void drawRoads()
    {
        this.roads_parent = new GameObject("Roads Group");
        List<GameObject> objects = new List<GameObject>();

        foreach (Road r in this.roads)
        {
            objects.Add(r.getObject());
            r.draw();
        }

        StaticBatchingUtility.Combine(objects.ToArray(), this.roads_parent);

        this.roads_drawn = true;
    }

    public IEnumerator drawRoadsRoutine()
    {
        this.roads_parent = new GameObject("Roads Group");
        List<GameObject> objects = new List<GameObject>();

        foreach (Road r in this.roads)
        {
            objects.Add(r.getObject());
            r.draw();

            yield return null;
        }

        StaticBatchingUtility.Combine(objects.ToArray(), this.roads_parent);

        this.roads_drawn = true;
    }

    // 4 - Extract available lots to build on
    public void generateLots (bool write)
    {
        if(this.road_map != null)
        {
            this.city_lots = Tools.lotExtraction(this.road_map, this.width, this.length, this.seed, write);

            foreach (Lot l in this.city_lots) l.setSize(this.start);
        }
    }

    // 5 - Create height map(Use perlin filter)
    public void generateHeightMap (bool write)
    {
        int i, j;

        // Initiate height map
        this.height_map = new float[this.width][];
        for (i = 0; i < this.width; i++) this.height_map[i] = new float[this.length];

        Random.InitState(this.seed_num);
        for(i = 0; i < this.width; i++)
        {
            for(j = 0; j < this.length; j++)
            {
                this.height_map[i][j] = Random.value * (this.max_building_height - 1) + Mathf.PerlinNoise(i, j);
            }
        }

        if (write)
        {
            TextWriter tw = new StreamWriter("Height_Map_" + this.seed + ".txt");
            for (i = 0; i < this.width; i++)
            {
                for (j = 0; j < this.length; j++)
                {
                    tw.Write(" ");
                    tw.Write(this.height_map[i][j]);
                }
                tw.WriteLine();
            }
            tw.Flush();
        }

    }

    // 6 - Apply filter for each city lot (Assign category, type and height)
    public void assignLots ()
    {
        if(this.height_map != null)
        {
            Random.InitState(this.seed_num);

            //float res_value = 0.1f;
            float com_value = 0.48f;
            float sky_value = 0.52f;

            int index = 1;
            foreach (Lot l in this.city_lots)
            {
                float smallest = smallestHeight(l);

                if(smallest > sky_value)
                {
                    // Skyscraper area
                    l.setBuildingCategory(2, this.max_skyscraper_width, this.max_skyscraper_length, this.skyscraper_height_percentage * this.max_building_height);
                }
                else
                {
                    if(smallest > com_value)
                    {
                        // Comercial area
                        l.setBuildingCategory(1, this.max_comercial_width, this.max_comercial_length, this.comercial_height_percentage * this.max_building_height);
                    }
                    else
                    {
                        // Residencial area
                        l.setBuildingCategory(0, this.max_residential_width, this.max_residential_length, this.residential_height_percentage * this.max_building_height);
                    }
                }

                index++;
            }
        }
    }

    private float averageHeight (Lot l)
    {
        Vector3 corner = l.getCornerPoint();
        int width = Mathf.FloorToInt(l.getWidth());
        int length = Mathf.FloorToInt(l.getLength());

        int total = 0;
        float sum = 0;
        float average;

        int start_i = Mathf.FloorToInt(corner.x + 0.5f);
        int start_j = Mathf.FloorToInt(corner.z + 0.5f);

        int end_i = start_i + width;
        int end_j = start_j + length;

        for (int i = start_i; i < end_i; i++)
        {
            for (int j = start_j; j < end_j; j++)
            {
                sum += this.height_map[i][j];
                total++;
            }
        }

        average = sum / total;
        return average;
    }

    private float smallestHeight (Lot l)
    {
        Vector3 corner = l.getCornerPoint();
        int width = Mathf.FloorToInt(l.getWidth());
        int length = Mathf.FloorToInt(l.getLength());

        float smallest = 10.0f;

        int start_i = Mathf.FloorToInt(corner.x + 0.5f);
        int start_j = Mathf.FloorToInt(corner.z + 0.5f);

        int end_i = start_i + width;
        int end_j = start_j + length;

        for (int i = start_i; i < end_i; i++)
        {
            for (int j = start_j; j < end_j; j++)
            {
                float value = this.height_map[i][j];
                if (value < smallest)
                {
                    smallest = value;
                }
            }
        }

        return smallest;
    }

    private float biggestHeight (Lot l)
    {
        Vector3 corner = l.getCornerPoint();
        int width = Mathf.FloorToInt(l.getWidth());
        int length = Mathf.FloorToInt(l.getLength());

        float biggest = 0.0f;

        int start_i = Mathf.FloorToInt(corner.x + 0.5f);
        int start_j = Mathf.FloorToInt(corner.z + 0.5f);

        int end_i = start_i + width;
        int end_j = start_j + length;

        for (int i = start_i; i < end_i; i++)
        {
            for (int j = start_j; j < end_j; j++)
            {
                float value = this.height_map[i][j];
                if (value > biggest)
                {
                    biggest = value;
                }
            }
        }

        return biggest;
    }

    // 7 - Draw lot corners
    public void drawLotCorners ()
    {
        foreach (Lot l in this.city_lots)
        {
            l.drawCorners();
        }
    }

    public IEnumerator drawLotCornersRoutine()
    {
        foreach (Lot l in this.city_lots)
        {
            GameObject parent = new GameObject("Corners Group per Lot");
            List<GameObject> objects = new List<GameObject>();

            l.setCornersParent(parent);

            foreach (Tile t in l.getCorners())
            {
                objects.Add(t.getObject());
                t.draw();

                yield return null;
            }

            StaticBatchingUtility.Combine(objects.ToArray(), parent);
        }
    }

    // 8 - Draw lot category tile map
    public void drawLotTiles ()
    {
        foreach (Lot l in this.city_lots)
        {
            l.drawTile();
        }
    }

    public IEnumerator drawLotTilesRoutine()
    {
        foreach (Lot l in this.city_lots)
        {
            l.getTile().draw();

            yield return null;
        }
    }

    public void undrawLotTiles()
    {
        foreach (Lot l in this.city_lots)
        {
            l.undrawTile();
        }
    }

    // 9 - Generate available building points in each lot
    public void generateBuildingPoints ()
    {
        foreach (Lot l in this.city_lots)
        {
            l.setBuildingPoints();
        }
    }

    // 10 - Draw building points
    public void drawBuildingPoints()
    {
        foreach (Lot l in this.city_lots)
        {
            l.drawBuildingPoints();
        }
    }

    public IEnumerator drawBuildingPointsRoutine()
    {
        foreach (Lot l in this.city_lots)
        {
            GameObject parent = new GameObject("Building Points Group per Lot");
            List<GameObject> objects = new List<GameObject>();

            l.setTilesParent(parent);
            foreach (Tile t in l.getBuildingTilesPointer())
            {
                objects.Add(t.getObject());
                t.draw();

                yield return null;
            }

            StaticBatchingUtility.Combine(objects.ToArray(), parent);
        }
    }

    // 11 - Generate buildings
    public void generateBuildings()
    {
        foreach (Lot l in this.city_lots)
        {
            l.setBuildings(this.seed_num);
        }
    }

    // 12 - Draw buildings
    public void drawBuildings()
    {
        Random.InitState(this.seed_num);
        int area_type = 0;

        foreach (Lot l in this.city_lots)
        {
            if (l.hasBuildings())
            {
                l.setTileMaterial("DEFAULT");
                l.drawTile();
                l.drawBuildings();
            }
            else
            {
                area_type = Mathf.RoundToInt(Random.value);
                switch (area_type)
                {
                    case 0:
                        l.setTileMaterial("LIGHTGRAY");
                        break;
                    case 1:
                        l.setTileMaterial("DARKGREEN");
                        break;
                }
                l.drawTile();
            }
        }

        this.lots_drawn = true;
    }

    public IEnumerator drawBuildingsRoutine()
    {
        Random.InitState(this.seed_num);
        int area_type = 0;

        foreach (Lot l in this.city_lots)
        {
            if (l.hasBuildings())
            {
                GameObject parent = new GameObject("Buildings Group per Lot");
                List<GameObject> objects = new List<GameObject>();

                l.setTileMaterial("DEFAULT");
                l.drawTile();
                l.setBuildingsParent(parent);

                foreach (Building b in l.getBuildingsPointer())
                {
                    objects.Add(b.getObject());
                    b.draw();

                    yield return null;
                }

                StaticBatchingUtility.Combine(objects.ToArray(), parent);
            } else
            {
                area_type = Mathf.RoundToInt(Random.value);
                switch (area_type)
                {
                    case 0:
                        l.setTileMaterial("LIGHTGRAY");
                        break;
                    case 1:
                        l.setTileMaterial("DARKGREEN");
                        break;
                }
                l.drawTile();
            }

            yield return null;
        }

        this.lots_drawn = true;
    }
}

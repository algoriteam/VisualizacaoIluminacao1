﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile {
    private Vector3 position;
    private Quaternion rotation;
    private float length;
    private float width;

    private string name;
    private GameObject tile;

    public Tile(Vector3 p, Quaternion q, float w, float l)
    {
        this.position = p;
        this.rotation = q;
        this.length = l;
        this.width = w;

        setName();
        initObject();
        setMaterial("GRAY");
        setShape();
    }

    private void initObject()
    {
        this.tile = new GameObject(this.name);
        this.tile.isStatic = true;
        this.tile.transform.position = this.position;
        this.tile.transform.rotation = this.rotation;

        undraw();
    }

    public void draw()
    {
        this.tile.SetActive(true);
    }

    public void undraw()
    {
        this.tile.SetActive(false);
    }

    // GETS

    public Vector3 getPosition()
    {
        return this.position;
    }

    public Quaternion getQuaternion()
    {
        return this.rotation;
    }

    public float getLength()
    {
        return this.length;
    }

    public float getWidth()
    {
        return this.width;
    }

    public GameObject getObject()
    {
        return this.tile;
    }

    // SETS

    public void setName()
    {
        this.name = "Tile";
    }

    public void setMaterial(string color)
    {
        if (this.tile != null)
        {
            Material material;
            switch (color.ToUpper())
            {
                case "DEFAULT":
                    material = Resources.Load("Materials/Default", typeof(Material)) as Material;
                    break;
                case "BLUE":
                    material = Resources.Load("Materials/Blue", typeof(Material)) as Material;
                    break;
                case "ORANGE":
                    material = Resources.Load("Materials/Orange", typeof(Material)) as Material;
                    break;
                case "GREEN":
                    material = Resources.Load("Materials/Green", typeof(Material)) as Material;
                    break;
                case "DARKGREEN":
                    material = Resources.Load("Materials/DarkGreen", typeof(Material)) as Material;
                    break;
                case "PINK":
                    material = Resources.Load("Materials/Pink", typeof(Material)) as Material;
                    break;
                case "LIGHTGRAY":
                    material = Resources.Load("Materials/Light Gray", typeof(Material)) as Material;
                    break;
                case "MEDIUMGRAY":
                    material = Resources.Load("Materials/Light Gray", typeof(Material)) as Material;
                    break;
                default:
                    material = Resources.Load("Materials/Gray", typeof(Material)) as Material;
                    break;
            }
            if (this.tile.GetComponent<MeshRenderer>() == null)
            {
                this.tile.AddComponent<MeshRenderer>().material = material;
            }
            else
            {
                this.tile.GetComponent<MeshRenderer>().material = material;
            }
        }
    }

    private void setShape()
    {
        if (this.tile != null)
        {
            // Set model shape
            Mesh mesh = new Mesh();
            mesh.vertices = GetVertices();
            mesh.triangles = GetTriangles();
            mesh.uv = GetUVsMap();

            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            // Set model shape
            this.tile.AddComponent<MeshFilter>().mesh = mesh;

            // Set collider
            this.tile.AddComponent<MeshCollider>();

        }
    }

    // AUXILIARY

    private Vector3[] GetVertices()
    {
        // Vertices
        Vector3 vertice_0 = new Vector3(this.width, 0.0f, 0.0f);
        Vector3 vertice_1 = new Vector3(this.width, 0.0f, this.length);
        Vector3 vertice_2 = new Vector3(0.0f, 0.0f, this.length);
        Vector3 vertice_3 = Vector3.zero;

        Vector3[] vertices = {
            vertice_0, vertice_1, vertice_2, vertice_3
        };

        return vertices;
    }

    private Vector3[] GetNormals()
    {
        Vector3 up = Vector3.up;
        Vector3 down = Vector3.down;

        Vector3[] normals = {
            // UP Side Render
            up, up, up, up
        };

        return normals;
    }

    private int[] GetTriangles()
    {
        int[] triangles = {
            // Up
            0, 3, 2,
            0, 2, 1
        };

        return triangles;
    }

    private Vector2[] GetUVsMap()
    {
        Vector2 _00_CORDINATES = new Vector2(0f, 0f);
        Vector2 _10_CORDINATES = new Vector2(1f, 0f);
        Vector2 _01_CORDINATES = new Vector2(0f, 1f);
        Vector2 _11_CORDINATES = new Vector2(1f, 1f);

        Vector2[] uvs = {
            // Top
            _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES
        };

        return uvs;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road {

    private Vector3 position;
    private Quaternion rotation;
    private int type;

    private string name;
    private GameObject road;

    public Road(Vector3 p, Quaternion q, int t)
    {
        this.position = p;
        this.rotation = q;

        setType(t);
        setName();

        initObject();

        setMaterial();
        setShape();
    }

    private void initObject()
    {
        this.road = new GameObject(this.name);
        this.road.isStatic = true;
        this.road.transform.position = this.position;
        this.road.transform.rotation = this.rotation;

        undraw();
    }

    public void draw()
    {
        this.road.SetActive(true);
    }

    public void undraw()
    {
        this.road.SetActive(false);
    }

    // GETS

    public Vector3 getPosition()
    {
        return this.position;
    }

    public Quaternion getQuaternion()
    {
        return this.rotation;
    }

    public GameObject getObject()
    {
        return this.road;
    }

    // SETS

    public void setType(int t)
    {
        this.type = t;
    }

    public void setName()
    {
        switch (this.type)
        {
            case 1:
                this.name = "Road Normal";
                break;
            case 2:
                this.name = "Road Intersection";
                break;
            case 3:
                this.name = "Road T";
                break;
        }
    }

    public void setMaterial()
    {
        if (this.road != null)
        {
            Material material_road;
            Material material_sidewalk = Resources.Load("Materials/Medium Gray", typeof(Material)) as Material; ;
            switch (this.type)
            {
                case 1:
                    material_road = Resources.Load("Materials/Gray", typeof(Material)) as Material;
                    break;
                case 2:
                    material_road = Resources.Load("Materials/Blue", typeof(Material)) as Material;
                    break;
                case 3:
                    material_road = Resources.Load("Materials/Green", typeof(Material)) as Material;
                    break;
                default:
                    material_road = Resources.Load("Materials/Gray", typeof(Material)) as Material;
                    break;
            }
            this.road.AddComponent<MeshRenderer>().materials = new Material[]{ material_sidewalk, material_road};
        }
    }

    private void setShape()
    {
        if (this.road != null)
        {
            // Set model shape
            Mesh mesh = new Mesh();
            mesh.subMeshCount = 2;

            mesh.vertices = GetVertices();

            int[] triangles_0 = GetTriangles(0);
            int[] triangles_1 = GetTriangles(1);

            mesh.SetTriangles(triangles_0, 0);
            mesh.SetTriangles(triangles_1, 1);
            mesh.uv = GetUVsMap();


            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            // Set model shape
            this.road.AddComponent<MeshFilter>().mesh = mesh;

            // Set collider
            this.road.AddComponent<MeshCollider>();
        }
    }

    // AUXILIARY

    private Vector3[] GetVertices()
    {
        // Vertices
        float half_size = 0.5f;
        float quad_size = 0.25f;

        Vector3[] vertices = {};

        Vector3 vertice_0;
        Vector3 vertice_1;
        Vector3 vertice_2;
        Vector3 vertice_3;
        Vector3 vertice_4;
        Vector3 vertice_5;
        Vector3 vertice_6;
        Vector3 vertice_7;
        Vector3 vertice_8;
        Vector3 vertice_9;
        Vector3 vertice_10;
        Vector3 vertice_11;
        Vector3 vertice_12;
        Vector3 vertice_13;
        Vector3 vertice_14;
        Vector3 vertice_15;

        switch (this.type)
        {
            case 1:
                vertice_0 = new Vector3(half_size, 0.0f, -half_size);
                vertice_1 = new Vector3(half_size, 0.0f, -quad_size);
                vertice_2 = new Vector3(-half_size, 0.0f, -quad_size);
                vertice_3 = new Vector3(-half_size, 0.0f, -half_size);
                vertice_4 = new Vector3(half_size, 0.0f, quad_size);
                vertice_5 = new Vector3(half_size, 0.0f, half_size);
                vertice_6 = new Vector3(-half_size, 0.0f, half_size);
                vertice_7 = new Vector3(-half_size, 0.0f, quad_size);

                vertices = new Vector3[]
                {
                        // Left sidewalk
                        vertice_0, vertice_1, vertice_2, vertice_3,
                        // Right sidewalk
                        vertice_4, vertice_5, vertice_6, vertice_7,
                        // Road
                        vertice_1, vertice_4, vertice_7, vertice_2
                };
                break;
            case 2:
                vertice_0 = new Vector3(half_size, 0.0f, -half_size);
                vertice_1 = new Vector3(half_size, 0.0f, -quad_size);
                vertice_2 = new Vector3(quad_size, 0.0f, -quad_size);
                vertice_3 = new Vector3(quad_size, 0.0f, -half_size);
                vertice_4 = new Vector3(half_size, 0.0f, quad_size);
                vertice_5 = new Vector3(half_size, 0.0f, half_size);
                vertice_6 = new Vector3(quad_size, 0.0f, half_size);
                vertice_7 = new Vector3(quad_size, 0.0f, quad_size);
                vertice_8 = new Vector3(-quad_size, 0.0f, quad_size);
                vertice_9 = new Vector3(-quad_size, 0.0f, half_size);
                vertice_10 = new Vector3(-half_size, 0.0f, half_size);
                vertice_11 = new Vector3(-half_size, 0.0f, quad_size);
                vertice_12 = new Vector3(-quad_size, 0.0f, -half_size);
                vertice_13 = new Vector3(-quad_size, 0.0f, -quad_size);
                vertice_14 = new Vector3(-half_size, 0.0f, -quad_size);
                vertice_15 = new Vector3(-half_size, 0.0f, -half_size);

                vertices = new Vector3[]
                {
                        // Left top sidewalk
                        vertice_12, vertice_13, vertice_14, vertice_15,
                        // Left bottom sidewalk
                        vertice_0, vertice_1, vertice_2, vertice_3,
                        // Right top sidewalk
                        vertice_8, vertice_9, vertice_10, vertice_11,
                        // Right bottom sidewalk
                        vertice_4, vertice_5, vertice_6, vertice_7,
                        // Road 1
                        vertice_1, vertice_4, vertice_11, vertice_14,
                        // Road 2
                        vertice_3, vertice_6, vertice_9, vertice_12
                };
                break;
            case 3:
                vertice_0 = new Vector3(half_size, 0.0f, -half_size);
                vertice_1 = new Vector3(half_size, 0.0f, half_size);
                vertice_2 = new Vector3(quad_size, 0.0f, half_size);
                vertice_3 = new Vector3(quad_size, 0.0f, -half_size);
                vertice_4 = new Vector3(-quad_size, 0.0f, -half_size);
                vertice_5 = new Vector3(-quad_size, 0.0f, -quad_size);
                vertice_6 = new Vector3(-half_size, 0.0f, -quad_size);
                vertice_7 = new Vector3(-half_size, 0.0f, -half_size);
                vertice_8 = new Vector3(-quad_size, 0.0f, quad_size);
                vertice_9 = new Vector3(-quad_size, 0.0f, half_size);
                vertice_10 = new Vector3(-half_size, 0.0f, half_size);
                vertice_11 = new Vector3(-half_size, 0.0f, quad_size);

                vertices = new Vector3[]
                {
                        // Left sidewalk
                        vertice_4, vertice_5, vertice_6, vertice_7,
                        // Right sidewalk
                        vertice_8, vertice_9, vertice_10, vertice_11,
                        // Bottom sidewalk
                        vertice_0, vertice_1, vertice_2, vertice_3,
                        // Road 1
                        vertice_3, vertice_2, vertice_9, vertice_4,
                        // Road 2
                        vertice_5, vertice_8, vertice_11, vertice_6
                };
                break;
        }

        return vertices;
    }

    private Vector3[] GetNormals()
    {
        Vector3 up = Vector3.up;

        Vector3[] normals = {};

        switch (this.type)
        {
            case 1:
                normals = new Vector3[]
                {
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up
                };
                break;
            case 2:
                normals = new Vector3[]
                {
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up
                };
                break;
            case 3:
                normals = new Vector3[]
                {
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up,
                    // UP Side Render
                    up, up, up, up
                };
                break;
        }

        return normals;
    }

    private int[] GetTriangles(int mesh)
    {
        int[] triangles = {};

        switch (mesh)
        {
            case 0:
                switch (this.type)
                {
                    case 1:
                        triangles = new int[]
                        {
                            // Left sidewalk up
                            0, 3, 2,
                            0, 2, 1,

                            // Right sidewalk up
                            4, 7, 6,
                            4, 6, 5
                        };
                        break;
                    case 2:
                        triangles = new int[]
                        {
                            // Left bottom sidewalk up
                            0, 3, 2,
                            0, 2, 1,

                            // Right bottom sidewalk up
                            4, 7, 6,
                            4, 6, 5,

                            // Right top sidewalk up
                            8, 11, 10,
                            8, 10, 9,

                            // Left top sidewalk up
                            12, 15, 14,
                            12, 14, 13
                        };
                        break;
                    case 3:
                        triangles = new int[]
                        {
                            // Left sidewalk up
                            0, 3, 2,
                            0, 2, 1,

                            // Right sidewalk up
                            4, 7, 6,
                            4, 6, 5,

                            // bottom sidewalk up
                            8, 11, 10,
                            8, 10, 9,
                        };
                        break;
                }
                break;
            case 1:
                switch (this.type)
                {
                    case 1:
                        triangles = new int[] 
                        {
                            // Road up
                            8, 11, 10,
                            8, 10, 9
                        };
                        break;
                    case 2:
                        triangles = new int[]
                        {
                            // Road 1 up
                            16, 19, 18,
                            16, 18, 17,

                            // Road 2 up
                            20, 23, 22,
                            20, 22, 21
                        };
                        break;
                    case 3:
                        triangles = new int[]
                        {
                            // Road 1 up
                            12, 15, 14,
                            12, 14, 13,

                            // Road 2 up
                            16, 19, 18,
                            16, 18, 17
                        };
                        break;
                }
                break;
        }

        return triangles;
    }

    private Vector2[] GetUVsMap()
    {
        Vector2 _00_CORDINATES = new Vector2(0f, 0f);
        Vector2 _10_CORDINATES = new Vector2(1f, 0f);
        Vector2 _01_CORDINATES = new Vector2(0f, 1f);
        Vector2 _11_CORDINATES = new Vector2(1f, 1f);

        Vector2[] uvs = {};

        switch (this.type)
        {
            case 1:
                uvs = new Vector2[]
                {
                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES
                };
                break;
            case 2:
                uvs = new Vector2[]
                {
                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES
                };
                break;
            case 3:
                uvs = new Vector2[]
                {
                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,

                    // Top
                    _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES
                };
                break;
        }

        return uvs;
    }

}

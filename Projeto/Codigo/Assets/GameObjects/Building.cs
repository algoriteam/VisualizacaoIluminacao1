﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building {

    private Vector3 position;
    private Quaternion rotation;
    private int type;
    private float width;
    private float length;
    private float height;

    private string name;
    private GameObject building;

    public Building(Vector3 p, Quaternion q, int t, float w, float l, float h)
    {
        this.position = p;
        this.rotation = q;
        this.width = w;
        this.length = l;
        this.height = h;

        setType(t);

        switch (t)
        {
            case 0:
                setName("Residential");
                initObject();
                setMaterial("BLUE");
                break;
            case 1:
                setName("Comercial");
                initObject();
                setMaterial("ORANGE");
                break;
            case 2:
                setName("Skyscraper");
                initObject();
                setMaterial("GREEN");
                break;
            default:
                setName(Tools.RandomString(10));
                initObject();
                setMaterial("DEFAULT");
                break;
        }

        setShape();
    }

    private void initObject()
    {
        this.building = new GameObject(this.name);
        this.building.isStatic = true;
        this.building.transform.position = this.position;
        this.building.transform.rotation = this.rotation;

        undraw();
    }

    public void draw()
    {
        this.building.SetActive(true);
    }

    public void undraw()
    {
        this.building.SetActive(false);
    }

    // GETS

    public int getType()
    {
        return this.type;
    }

    public string getName()
    {
        return this.name;
    }

    public GameObject getObject()
    {
        return this.building;
    }

    // SETS

    public void setType(int t)
    {
        this.type = t;
    }

    public void setName(string c)
    {
        this.name = c;
    }

    public void setMaterial(string c)
    {
        if (this.building != null)
        {
            Material material;
            switch (c.ToUpper())
            {
                case "BLUE":
                    material = Resources.Load("Materials/Blue", typeof(Material)) as Material;
                    break;
                case "ORANGE":
                    material = Resources.Load("Materials/Orange", typeof(Material)) as Material;
                    break;
                case "GREEN":
                    material = Resources.Load("Materials/Green", typeof(Material)) as Material;
                    break;
                default:
                    material = Resources.Load("Materials/Default", typeof(Material)) as Material;
                    break;
            }

            this.building.AddComponent<MeshRenderer>().material = material;
        }
    }

    private void setShape()
    {
        if (this.building != null)
        {
            // Set model shape
            Mesh mesh = new Mesh();
            mesh.vertices = GetVertices();
            mesh.triangles = GetTriangles();
            mesh.uv = GetUVsMap();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();

            // Set model shape
            this.building.AddComponent<MeshFilter>().mesh = mesh;

            // Set collider
            this.building.AddComponent<MeshCollider>();
        }
    }

    // AUXILIARY

    private Vector3[] GetVertices()
    {
        float half_width = this.width * 0.5f;
        float half_length = this.length * 0.5f;

        // Vertices
        Vector3 vertice_0 = new Vector3(half_width, 0.0f, -half_length);
        Vector3 vertice_1 = new Vector3(half_width, 0.0f , half_length);
        Vector3 vertice_2 = new Vector3(half_width, this.height, half_length);
        Vector3 vertice_3 = new Vector3(half_width, this.height, -half_length);
        Vector3 vertice_4 = new Vector3(-half_width, this.height, -half_length);
        Vector3 vertice_5 = new Vector3(-half_width, this.height, half_length);
        Vector3 vertice_6 = new Vector3(-half_width, 0.0f, half_length);
        Vector3 vertice_7 = new Vector3(-half_width, 0.0f, -half_length);

        Vector3[] vertices = new Vector3[]
        {
            // Front
            vertice_2, vertice_3, vertice_0, vertice_1,

            // Left
            vertice_3, vertice_4, vertice_7, vertice_0,

            // Right
            vertice_5, vertice_2, vertice_1, vertice_6,

            // Back
            vertice_4, vertice_5, vertice_6, vertice_7,

            // Top
            vertice_2, vertice_5, vertice_4, vertice_3
        };

        return vertices;
    }

    private Vector3[] GetNormals()
    {
        Vector3 up = Vector3.up;
        Vector3 down = Vector3.down;
        Vector3 front = Vector3.forward;
        Vector3 back = Vector3.back;
        Vector3 left = Vector3.left;
        Vector3 right = Vector3.right;

        Vector3[] normals = new Vector3[]
        {
            // LEFT Side Render
            left, left, left, left,
                    
            // FRONT Side Render
            front, front, front, front,
                    
            // BACK Side Render
            back, back, back, back,
                    
            // RIGTH Side Render
            right, right, right, right,
                    
            // UP Side Render
            up, up, up, up
        };

        return normals;
    }

    private int[] GetTriangles()
    {
        int[] triangles = 
        {   
            // Cube Left Side
            3, 1, 0,
            3, 2, 1,
            // Cube Front Side
            7, 5, 4,
            7, 6, 5,
            // Cube Back Side
            11, 9, 8,
            11, 10, 9,
            // Cube Right Side
            15, 13, 12,
            15, 14, 13,
            // Cube Top Side
            19, 17, 16,
            19, 18, 17
        };

        return triangles;
    }

    private Vector2[] GetUVsMap()
    {
        Vector2 _00_CORDINATES = new Vector2(0f, 0f);
        Vector2 _10_CORDINATES = new Vector2(1f, 0f);
        Vector2 _01_CORDINATES = new Vector2(0f, 1f);
        Vector2 _11_CORDINATES = new Vector2(1f, 1f);

        Vector2[] uvs = new Vector2[]
        {
            // Left
            _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
            // Front
            _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
            // Back
            _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
            // Right
            _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES,
            // Top
            _11_CORDINATES, _01_CORDINATES, _00_CORDINATES, _10_CORDINATES
        };

        return uvs;
    }

}

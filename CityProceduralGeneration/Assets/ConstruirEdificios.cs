﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstruirEdificios : MonoBehaviour
{

    public GameObject[] edificios;
    public GameObject ruasX;
    public GameObject ruasZ;
    public GameObject cruzamento;
    public int larguraC = 20;
    public int comprimentoC = 20;
    int[,] grelhaMapa;
    public int pegadaEdificio = 3;

    // Use this for initialization
    void Start()
    {
        grelhaMapa = new int[larguraC, comprimentoC];
        int x = 0, z = 0, res;// n;
        float aux = Random.Range(0,100);
        Vector3 pos;

        //Gerar Grelha
        for (int l = 0; l < larguraC; l++)
        {
            for (int c = 0; c < comprimentoC; c++)
            {
                grelhaMapa[c,l] = (int) (Mathf.PerlinNoise(l / 10.0f + aux, c / 10.0f + aux) * 10);
            }
        }

        //Construir Ruas
        for (int i = 0; i < 50; i++)
        {
            for (int l = 0; l < larguraC; l++)
            {
                grelhaMapa[x, l] = -1;
            }
            x += Random.Range(2, 20);
            if (x >= larguraC) break;
        }

        for (int i = 0; i < 50; i++)
        {
            for (int c = 0; c < comprimentoC; c++)
            {
                if (grelhaMapa[c, z] == -1) //colocar um cruzamento
                    grelhaMapa[c, z] = -3;
                else
                    grelhaMapa[c, z] = -2;
            }
            z += Random.Range(5, 20);
            if (z >= comprimentoC) break;
        }

        //Gerar Cidade
        for (int l = 0; l < larguraC; l++)
        {
            for (int c = 0; c < comprimentoC; c++)
            {
                res = grelhaMapa[c, l];
                pos = new Vector3(l * pegadaEdificio, 0, c*pegadaEdificio);

                if(res < -2)
                    Instantiate(cruzamento, pos, cruzamento.transform.rotation);
                else if (res < -1)
                    Instantiate(ruasZ, pos, ruasZ.transform.rotation);
                else if (res < 0)
                    Instantiate(ruasX, pos, ruasX.transform.rotation);
                else if (res < 1)
                    Instantiate(edificios[0], pos, Quaternion.identity);
                else if (res < 2)
                    Instantiate(edificios[1], pos, Quaternion.identity);
                else if (res < 3)
                    Instantiate(edificios[2], pos, Quaternion.identity);
                else if (res < 4)
                    Instantiate(edificios[3], pos, Quaternion.identity);
                else if (res < 6)
                    Instantiate(edificios[4], pos, Quaternion.identity);
                else if (res < 8)
                    Instantiate(edificios[5], pos, Quaternion.identity);
                else
                    Instantiate(edificios[6], pos, Quaternion.identity);
            }
        }

        /*for (int l = 0; l < larguraC; l++)
        {
            for (int c = 0; c < comprimentoC; c++)
            {
                res = (int)(Mathf.PerlinNoise(l/10.0f + aux, c/10.0f + aux) * 10);  
                Vector3 pos = new Vector3(l * pegadaEdificio, 0, c*pegadaEdificio);

                //n = Random.Range(0, edificios.Length);
                //Instantiate(edificios[n], pos, Quaternion.identity);

                if(res < 1)
                    Instantiate(edificios[0], pos, Quaternion.identity);
                else if (res < 2)
                    Instantiate(edificios[1], pos, Quaternion.identity);
                else if (res < 3)
                    Instantiate(edificios[2], pos, Quaternion.identity);
                else if (res < 4)
                    Instantiate(edificios[3], pos, Quaternion.identity);
                else if (res < 6)
                    Instantiate(edificios[4], pos, Quaternion.identity);
                else if (res < 8)
                    Instantiate(edificios[5], pos, Quaternion.identity);
                else
                    Instantiate(edificios[6], pos, Quaternion.identity);
            }
        }*/


    }

    // Update is called once per frame
    void Update()
    {

    }
}
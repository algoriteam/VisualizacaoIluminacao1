﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Teste : MonoBehaviour
{

    
    public int maxZ = 10;
    public int maxX = 10;


    // Use this for initialization
    void Start()
    {
        int[][] grelhaMapa = new int[maxZ][];
        int z = 0, x = 0, i, j;
        int gLevel = 6;

        //Inicializar Grelha
        for (i = 0; i < maxZ; i++)
        {
            grelhaMapa[i] = new int[maxX];
            for (j = 0; j < maxX; j++)
            {
                if ((i == 0 && (j == 0 || j == maxX - 1)) || (i == maxZ - 1 && (j == 0 || j == maxX - 1))) grelhaMapa[i][j] = 3;
                else if (j == 0 || j == maxZ - 1) grelhaMapa[i][j] = 1;
                else if (i == 0 || i == maxX - 1) grelhaMapa[i][j] = 2;
                else grelhaMapa[i][j] = 0;
            }
        }

        if (gLevel > 1)
        {
            z = Random.Range(2, maxZ - 3);
            x = 0;
            gLevel--;

            grelhaMapa[x][z] = 3;

            for (i = x + 1; i < maxX - 1 && grelhaMapa[i][z] == 0; i++)
            {
                grelhaMapa[i][z] = 1;
            }

            grelhaMapa[i][z] = 3;
            
            if (gLevel > 1 && 3 < maxX - 3)
            {
                growHe(grelhaMapa, Random.Range(2, maxX - 3), z, gLevel - 1);
                growHd(grelhaMapa, Random.Range(2, maxX - 3), z, gLevel - 1);
            }
        }

        using (TextWriter tw = new StreamWriter("test.txt"))
        {
            for (i = 0; i < maxZ; i++)
            {
                for (j = 0; j < maxX; j++)
                {
                    tw.Write(" ");
                    tw.Write(grelhaMapa[i][j]);
                }
                tw.WriteLine();
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    void growHe(int[][] grelhaMapa, int x, int z, int gLevel)
    {
        int j;

        grelhaMapa[x][z] = 3;

        if (z - 1 > 0) grelhaMapa[x][z - 1] = 2;

        for (j = z - 2; j > 0 && grelhaMapa[x][j] == 0; j--)
        {
            grelhaMapa[x][j] = 2;
        }

        grelhaMapa[x][j] = 3;

        if (gLevel > 1 && j + 2 <= z - 2)
        {
            growVs(grelhaMapa, x, Random.Range(j + 2, z - 2), gLevel - 1);
            growVi(grelhaMapa, x, Random.Range(j + 2, z - 2), gLevel - 1);
        }

    }

    void growHd(int[][] grelhaMapa, int x, int z, int gLevel)
    {
        int j;

        grelhaMapa[x][z] = 3;

        if (z + 1 < maxZ - 1) grelhaMapa[x][z + 1] = 2;

        for (j = z + 2; j < maxZ - 1 && grelhaMapa[x][j] == 0; j++)
        {
            grelhaMapa[x][j] = 2;
        }

        grelhaMapa[x][j] = 3;

        if (gLevel > 1 && z + 2 <= j - 2)
        {
            growVs(grelhaMapa, x, Random.Range(z + 2, j - 2), gLevel - 1);
            growVi(grelhaMapa, x, Random.Range(z + 2, j - 2), gLevel - 1);
        }

    }

    void growVs(int[][] grelhaMapa, int x, int z, int gLevel)
    {
        int i;

        grelhaMapa[x][z] = 3;

        if (x - 1 > 0) grelhaMapa[x - 1][z] = 1;

        for (i = x - 2; i > 0 && grelhaMapa[i][z] == 0; i--)
        {
            grelhaMapa[i][z] = 1;
        }

        grelhaMapa[i][z] = 3;

        if (gLevel > 1 && i + 2 < x - 2)
        {
            growHe(grelhaMapa, Random.Range(i + 2, x - 2), z, gLevel - 1);
            growHd(grelhaMapa, Random.Range(i + 2, x - 2), z, gLevel - 1);
        }

    }

    void growVi(int[][] grelhaMapa, int x, int z, int gLevel)
    {
        int i;

        grelhaMapa[x][z] = 3;

        if (x + 1 < maxX - 1) grelhaMapa[x + 1][z] = 1;

        for (i = x + 2; i < maxX - 1 && grelhaMapa[i][z] == 0; i++)
        {
            grelhaMapa[i][z] = 1;
        }

        grelhaMapa[i][z] = 3;

        if (gLevel > 1 && x + 2 < i - 2)
        {
            growHe(grelhaMapa, Random.Range(x + 2, i - 2), z, gLevel - 1);
            growHd(grelhaMapa, Random.Range(x + 2, i - 2), z, gLevel - 1);
        }

    }
}
